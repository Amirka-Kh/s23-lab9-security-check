# Lab9 -- Security check


## Introduction

You will or you already have been creating applications that are rather sensitive to security so you want to check that your application is secure enough. And maybe you think that you are not competent enough for security testing or maybe the cost of failure is rather big in your case, but usually you want to delegate this work to the other institution to let them say whether the system is secure enough. And in this lab, you would be that institution. ***Let's roll!***

## OWASP

[OWASP](https://owasp.org/) is Open Web Application Security Project. It is noncommercial organization that works to improve the security of software. It creates several products and tools, which we will use today and I hope you will at least take into account in future.

## OWASP Top 10

[OWASP Top 10](https://owasp.org/www-project-top-ten/) is a project that highlights 10 current biggest risks in software security. Highlighting that they make it visible. And forewarned is forearmed. As a software developer, you may at least consider that risks.

## OWASP Cheat Sheet Series

[OWASP CSS](https://cheatsheetseries.owasp.org/) is a project that accumulates the information about security splitted into very specific topics. And sometimes that might answer the question "How to make it secure?" according to specific topics (e.g. forgot password functionality).

## Google security check

Let's check how Google responds to the OWASP Cheatsheets of Forgot Password topic.

### Select the requirement from cheatsheets

E.g. how Google provides the forgot password feature and whether it "Return a consistent message for both existent and non-existent accounts."([link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)). To check this we should only try to reset password for existent and unexistent user and compare result.

Note: _In your work is important to make link to particlular requirement in cheatsheet._

### Create and execute test case

Up to this moment we do not know what would be during testing forgot password feature at google.com. Not only real result, but even steps. But we can expect some steps in this process and some expected result:
1. Beforehand we have to know some existent account. 
2. Open google.com page
3. Try to authenticate
4. Understand that we forgot the password
5. Try to reset password for some unexistent account
6. Remember the result
7. Go back and make the same for the prepared account
8. Remember the result 
9. Expect that results for 2 users would be indistinguishable, or the intruder might process [user enumeration atack](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=user%20enumeration%20attack)

Note: _You should not provide this high level steps, you might take it into account and go to next step to documenting test case execution_

Now lets try to execute that with documenting each step, to let yourself or someone another to reproduce it. During execution some unexpected issues might happen. It is ok, if the planned test case is not changd a lot just mention it into result section. Or if original plan changes a lot then return to it, review it, change if needed and continue.

| Test step  | Result |
|---|---|
| Open google.com  | Ok |
| Push sign in button  | Login page opened. If you have already logged in log out and repeate  |
| Push forgot email  |  There is no forgot password, but we can push forgot email. It still starts password recovery |
| Open new tab with sign in page  |  I tried to input unexistent account and understand that i do not know whether the account exists. So I would try to register with new account without submiting the form, so i would be sure about email that does not exists |
| Push create account button  |  Ok |
| Push create for yourself tab  |  Ok, create account form opened |
| Input your existent account email into email field  |  Ok, field highlight red |
| Try to change this email to make it unique  |  Ok, also you can input some name and page offer you new unique email. Remember that email |
| Return to forgot password page and input the unexistent email and push submit  |  Ok, form with first and last names opened |
| Input some first and last names and push submit |  Since you do not know the names of unexistent account it is ok to imagine somehting |
| See the result that there is no such an account  |  Ok, remember this result |
| Push retry button  |  Ok, we in the start of forgot password process |
| Input prepared existent email and push submit |  Ok, form with last and first name opened |
| Input first and last names and push submit  |  Ok, since it is prepared account you know its first and last names |
| See the page with confirmation  to send resetting email |  Ok. Remember this result |

Thats all, we tried 2 accounts and get 2 results, they are different so test case failed

### Analysis

Test case was failed, and according to different result intruders might process user enumeration attack and get database with existing users emails names and surnames, at least there is such an opportunity. Of course to brute force every email it require a lot of resources, but in this case it requires much more, because with email intruder need to pick up its name and surname. So even test case failed, that is not an security issue in given context.

Interesting thing that for my account, the email with resetting information would be sent on the same email(that i want to restore password)

Note: _Do not consider this result analysis as guide to action. Just see the test case result and provide your thoughts about_

## Homework Description

### Goal

To get familiar with OWASP CSS project by practicing limited security blackbox testing based on its materials. The default topics for testing is "Forgot password" and "Files Upload", but we are appeciate you to read another topics and select interesting one.

Note: _You are free to select topics or even combine topics, but remember that you should be able to test it using black box testing_

### Task

1. Pick a website that you can test using blackbox mode and put it into your solution and check that it is unique with previous submissions(web site and topic tested should be unique toghether, e.g. google.com might be tested on forgot password in one submission, and on file upload in another one). Like it was during [previous lab](https://gitlab.com/sqr-inno/s23-lab8-exploratory-testing-and-ui-testing)(If it is hard for you to find such website just google "Forgot password" and you will see list of forgot password pages, just select one for yourself, or if you select another topic, then google it to find websites and pages with another features)
2. Find specific advice/requirement/standard or similar that you want to check whether your picked website follows it. By default look into ["Forgot password"](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html) and ["File upload"](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html) topics, because there are very straight forward checklists, with possible black box testing. 

Note: _It is not mandatory to test Forgot Password feature, moreover it is strongly recommended to go through cheat sheet and find interesting features to test. Lab guide you for Forgot Password feature, only because I want to let every student to pass this lab. You are free(And I appreciate that) to select any topics (Be sure that you are able to ensure that during black box testing)_

3. For entity from previous stage imagine the flow of test(you do not need to document it, but you still can do it for more realism), its inputs, high level steps, and expected results.
4. Follow that flow with documenting each step and results
5. Comment the results of test cases. Is that everything ok, maybe it is a found vulnerability or weakness, or it might be intended weaking of the security protection in given context, what might be an alternative solution.(This is not a checklist that you must follow, just take it into account as line of thought)
6. Repeat from the first point(actually the website might be the same with different topic or entity from second point, and you do not need to mention web site each time) 4 times(the one from the lab is not counted). 

### Notes
1. Test cases might be executed fully manually, using helper tools, or fully automated, but in this cases I prefer to see the source code or know the used tool.
2. I want to see the linkage between entities from points 2,3,4,5 and with the referenced place from OWASP CSS(hope the structure would not change between your submission and my evaluation)
3. Some advices might be easy to test(e.g. password form should contain minimum password length validation), but some of them might require a lof of efforts(e.g. Ensure that the time taken for the user response message is uniform for existent user and unexistent). So 4 tests is just a recomendation it might be higher or lower. Finnaly I can reject submission if see too less effort put into the work. So to write me directly to prereview selected topics and proposed flows might be good idea(but not mandatory).

## Homework 

I chose [tumblr](https://www.tumblr.com/) website for test purposes.
I decided to check "Forgot password" and "Upload file" topics.

Procedure for "Forgot password" look like this:
1. Open tumblr login page.
2. Authenticate with email.
3. Use non-existing email, remember the result.
4. Use existing email.
5. Try to reset password for some existent account
6. Remember the result.
7. Go back and make the same for the not-resigtered account
8. Remember the result 
9. Expect that results for existing and not-registered users would be distinguishable.


| Test step  | Result |
|---|---|
| Open tumblr.com  | Ok |
| Push enter button  | Login window opened. |
| Submit non-existing email  |  Only valid emails are allowed |
| Submit existing email  |  I input existent account which was registered before, I proceeded with next page to submit password |
| Write wrong password | Email or password is wrong |

The message reveals what password is wrong, even if message says for both email and password. Password entering page is available only in case if the account with such email exists.

| Test step  | Result |
|---|---|
| Push forgot password |  Ok |
| Receive mail to change password |  Ok |
| Change password |  Ok |

After changing the password tumblr automatically logins client to the server, which opens gate to vulnerabilities according to OWASP CSS.

| Test step  | Result |
|---|---|
| Submit not-registered email  |  Ok, create password form opened |
| input passwords  |  Ok |

The test case failed because attacker easily distinguish which email exists, which email registered in the service, and which not.

Procedure for "Upload file" look like this:
1. Authorize in tumblr.
2. Select file upload option "photo".
3. Upload zip/js/exe/mov/avi/php/php%00.jpg file.
4. Select file upload option "link".
5. Upload malicious link.
6. Select file upload option "audio".
7. Upload zip/js/exe/mov/avi/php/php%00.jpg file.
8. Select file upload option "video".
9. Upload zip/js/exe/php/php%00.jpg file.
10. Add phishing link for video.
11. Add youtube link for video.

| Test step  | Result |
|---|---|
| Open tumblr.com as registered user | Ok |
| Select 'text' option and chose various file upload options | upload gif,video,photo services available. |
| Upload data which do not match gif,video or photo format  |  Data is not uploaded, no warning messages. |
| Select 'photo' option and chose from device options | Ok |
| Upload data which do not match photo\img format  | Error window is displayed, for `pic.php%00.jpg different error message displayed. |
| Select 'link' option | Ok |
| Upload data (xml\xss) which do not match link  | Data is not uploaded, no warning messages. |
| Add malicious link  | Link is added, no message printed. |
| Select 'audio' option and chose from device options | Ok |
| Upload data which do not match audio format  | Error window is displayed |
| Add audio link via such option | Audio link is added and Audio preview is displayed |
| Add malicious link via same option | link is added, publication option is not allowed |
| Add audio link and malicious link via same option | publication option is allowed |
| Select 'video' option and chose from device options | Ok |
| Upload data which do not match video format  | Error window is displayed |
| Add video link via such option | Video link is added and video preview is displayed |
| Add malicious link via same option | link is Highlighted with red, publication option is not allowed |
| Add video link and malicious link via same option | malicious link is not Highlighted with red, publication option is allowed |

The test indicates that tumblr website has good filtering for files. In addition, upload is allowed only by authorized users. However, there is no check for malicious link. Thus, there is possibility for CSRF attack. Also, I did not check ability for uploading images and video with malicious information in data.

Procedure for "Forgot password" in [5verst](https://my.5verst.ru/) look like this:
1. Enter private room in 5verst.
2. input wrong login.
3. input wrong password.
4. Select forgot password option.
5. Enter non-existing email.
6. Enter existing email.
7. Enter registered email.

| Test step  | Result |
|---|---|
| Open 5verst.ru  | Ok |
| Push enter button  | Login window opened. |
| Enter non-existing logic | invalid login\password |
| Enter wrong password | invalid login\password |
| Push forgot password | Ok |
| Submit non-existing email  | Only valid\real emails are allowed |
| Submit existing email  | Only registered emails are allowed |
| Submit registered email  | window with notification is apperead |

In 5verst website attacker will not know which login exists and if login or password is wrong, which will make brute-force attack very hard. However login consist from numbers only. In addition, in "forgot password" section, attacker can define which email present in the system. But, login is done via ID which are sent by email, thus for attackers breaking authentication becomes hard.
